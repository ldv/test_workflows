#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: CommandLineTool

requirements:
- class: InlineJavascriptRequirement

inputs:
- id: file
  type: File
  inputBinding:
    position: 1

outputs:
- id: paths
  type: string
  outputBinding:
    glob: std.out
    outputEval: $(self[0].contents.split('\n')[0])
    loadContents: true
stdout: std.out

baseCommand:
- readlink
- -f
