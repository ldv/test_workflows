#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: CommandLineTool

requirements:
- class: InitialWorkDirRequirement
  listing:
  - entryname: script.sh
    entry: |
      #!/bin/bash

      mkdir out
      touch out/one
      echo "Test" > out/two

inputs: []

outputs:
- id: dirout
  type: Directory
  outputBinding:
    glob: out

baseCommand:
- bash
- script.sh
