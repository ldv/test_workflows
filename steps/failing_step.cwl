#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: CommandLineTool

requirements:
- class: InitialWorkDirRequirement
  listing:
  - entryname: script.sh
    entry: |-
      #!/bin/bash

      echo "This is a test script that should fail" > temp.txt

      echo "My stdout" >&2
      echo "My stderr" >&1

      exit 1

inputs: []

outputs: []
stdout: std.out
stderr: std.err
