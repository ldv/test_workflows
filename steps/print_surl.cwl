#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: CommandLineTool

requirements:
- class: InitialWorkDirRequirement
  listing:
  - entryname: script.sh
    entry: |
      #!/bin/bash

      if [ $1 != '' ] 
      then
         echo "String is $1"
         exit 0
      else
         echo "String $1" is empty
         exit 1
      fi 

inputs:
- id: surl
  type: string
  inputBinding:
    position: 1

outputs: []

baseCommand:
- bash
- script.sh
