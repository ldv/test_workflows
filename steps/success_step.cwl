#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: CommandLineTool

requirements:
- class: InitialWorkDirRequirement
  listing:
  - entryname: script.sh
    entry: |-
      #!/bin/bash

      echo "This is a test script that should succeed" > temp.txt

      echo "My stdout" >&2
      echo "My stderr" >&1

      exit 0

inputs: []

outputs: []
stdout: std.out
stderr: std.err
