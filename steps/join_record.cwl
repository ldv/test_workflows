#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: ExpressionTool

requirements:
- class: InlineJavascriptRequirement

inputs:
- id: join
  type:
    type: array
    items:
      type: record
      fields:
      - name: data
        type: File[]

outputs:
- id: outputs
  type: File[]
expression: |-
  ${

    var outputs = []
    for(var record_id in inputs.join ){
        outputs = outputs.concat(inputs.join[record_id].data)
    }

    return {'outputs': outputs}
  }
