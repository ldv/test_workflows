#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: CommandLineTool

requirements:
- class: InitialWorkDirRequirement
  listing:
  - entryname: script.sh
    entry: |-
      #!/bin/bash
      echo This is random $RANDOM

inputs:
- id: name
  type: string

outputs:
- id: output
  type: File
  outputBinding:
    glob: '*.txt'
stdout: $(inputs.name).txt

baseCommand:
- bash
- script.sh
