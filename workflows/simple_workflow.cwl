#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: Workflow

requirements:
- class: ScatterFeatureRequirement

inputs:
- id: surls
  type: string[]

outputs:
- id: data
  type: File[]
  outputSource:
  - generate_files/output

steps:
- id: generate_files
  in:
  - id: name
    source: surls
  scatter: name
  run: ../steps/create_file.cwl
  out:
  - output
