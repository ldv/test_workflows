#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: Workflow

requirements:
- class: ScatterFeatureRequirement

inputs:
- id: data
  type: File[]

outputs:
- id: paths
  type: string[]
  outputSource: print_surl/paths

steps:
- id: print_surl
  in:
  - id: file
    source: data
  scatter: file
  run: ../steps/print_file.cwl
  out:
  - paths
