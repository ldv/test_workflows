#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: Workflow

inputs:
- id: surls
  type: string[]

outputs: []

steps:
- id: print_surl
  in:
  - id: surl
    source: surls
  scatter: surl
  run: ../steps/print_surl.cwl
  out: []
requirements:
- class: ScatterFeatureRequirement