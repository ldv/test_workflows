#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: Workflow

inputs:
- id: join
  type:
    type: array
    items:
      type: record
      fields:
      - name: data
        type: File[]

outputs:
- id: outputs
  type: File[]
  outputSource: join_record/outputs

steps:
- id: join_record
  in:
  - id: join
    source: join
  run: ../steps/join_record.cwl
  out:
  - outputs
