#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: Workflow

inputs: []

outputs:
- id: outdir
  type: Directory
  outputSource:
  - generate_directory/dirout

steps:
- id: generate_directory
  in: []
  run: ../steps/generate_directory.cwl
  out:
  - dirout
