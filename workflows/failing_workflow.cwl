#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: Workflow

inputs: []

outputs: []

steps:
- id: fail
  in: []
  run: ../steps/failing_step.cwl
  out: []
