# Test workflows 


Here are collected a list of workflows that are used to test if ATDB ldv services functionality is as expected.

In particular, here there will be workflows ending successfully and workflow ending with a processed failed. 
The ideas is to be able to test all the corner case of the ATDB infrastructure so that during new releases we can test its functionality.

## Current workflows
1. [Failing workflow](#failing-workflow)
2. [Workflow with output directory](#workflow-with-output-directory)
3. [Workflow with data as input](#workflow-with-data-as-input)
4. [Workflow with SURL as input](#workflow-with-SURL-as-input)
### Failing workflow

The failing workflow is a one step workflow contained in the directory
`workflows/failing_workflow.cwl`

It consists of only one step located in `steps/failing_step.cwl`
such step is supposed to create a temporary file that never gets returned to the user as an output and and error. This is meant to 
check the current functionality.

- A task will be scheduled by the executor on slurm
- The workflow should be checked out in the configured workflow directory on the cluster
- In the task work directory two files are generated inputs.json and script.sh
- Upon ran, a third file will be located in the task work directory such files is a tar archive with the temporary directory of all the run of the workflow called tmp.tar
- Inside the tmp.tar archive will be at least one version of the file temp.txt (as of 4/4/2023 the executor retries 3 times so there should be 3 version of this file in different temp directories)
- Inside the tmp.tar archive there should be additionally two files with stdout and stderr (more depending on the retries sets)
- the task should be set to failed in atdb


Here is how the task is specified in ATDB:
```
{
    "id": 21,
    "description": "Test",
    "workflow_uri": "test_failing_workflow",
    "repository": "https://git.astron.nl/ldv/test_workflows",
    "commit_id": "main",
    "path": "workflows/failing_workflow.cwl",
    "oi_size_fraction": 0.0,
    "meta_scheduling": null,
    "default_parameters": null,
    "prefetch": false
}
```

It can also be seen at the url https://sdc.astron.nl:5554/atdb/workflows/21/

An example task is:
```
{
    "id": 30689,
    "task_type": "regular",
    "creationTime": "2023-04-04T12:34:18.624592Z",
    "filter": "ldv-spec:74",
    "predecessor": null,
    "predecessor_status": "no_predecessor",
    "successors": [],
    "project": "TEST",
    "sas_id": "12345",
    "priority": 100,
    "purge_policy": "no",
    "resume": true,
    "workflow": 21,
    "stage_request_id": 40951,
    "status": "defined",
    "new_status": "defined",
    "quality": "",
    "inputs": [],
    "outputs": null,
    "metrics": {},
    "remarks": null,
    "status_history": [
        "defining (2023-04-04T12:34:18Z)",
        "defined (2023-04-04T12:35:56Z)"
    ],
    "size_to_process": 0,
    "size_processed": 0,
    "total_processing_time": 46,
    "log_entries": [],
    "meta_scheduling": null,
    "environment": "",
    "archive": null
}
```

As the workflow such task can be also inspected at
https://sdc.astron.nl:5554/atdb/tasks/30689/
and (unlike a workflow) at the url
https://sdc.astron.nl:5554/atdb/task_details/30689/1


### Workflow with output directory
The workflow is a one step workflow contained in the directory `workflows/workflow_with_outputdirectory.cwl`

It consists of only one step located in `steps/generate_directory.cwl` such step creates a directory. This is meant to check the current capability of the datamanager in handling the directory output.
### Workflow with data as input
The workflow is a one step workflow contained in the directory `workflows/data_input_workflow.cwl`

It consists of only one step located in `steps/print_file.cwl` which print in the output the name of the file. This is meant to debug the input from the datamanager when a file is passed instead of a SURL
### Workflow with SURL as input
The workflow is a one step workflow contained in the directory `workflows/surl_input_workflow.cwl`

It consists of only one step located in `steps/print_surl.cwl` which print in the output the input surl string. This is meant to debug the input from the datamanager when a surl is passed to workflow as input.

### Simple workflow 
This workflow takes a list of strings and outputs a list of files 
It consists of only one step located in `steps/create_file.cwl` which produces a file with a random content and a file name which is based on the input string. 
### Join workflow
This workflow tests the joining functionality added to the ATDB workflow. In particular, the workflow is configured to join the output of tasks with the output produced by the simple workflow. This workflow will return a list of files.